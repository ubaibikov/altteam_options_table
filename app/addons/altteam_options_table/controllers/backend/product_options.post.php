<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @module     "Alt-team: Table of options"
 * @version    4.0.x 
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if ($mode == "update_option_tables_by_name") {

	    // only for product with 2 options and both options names are same as input names
		if ( (!isset($_REQUEST['table']['R']) || empty($_REQUEST['table']['R'])) && (!isset($_REQUEST['table']['C']) || empty($_REQUEST['table']['C']))) {
		
			fn_set_notification('W', __('warning'), __('enter_both_values'));

		    return array(CONTROLLER_STATUS_OK, "product_options.pre_update_option_tables_by_name");
		}

	    return array(CONTROLLER_STATUS_OK, "product_options.update_option_tables_by_name?rows=" . $_REQUEST['table']['R']
	    	 . "&columns=" . $_REQUEST['table']['C']
	    	 . "&categories=" . $_REQUEST['table']['categories']);
	  
	}
}

if ($mode == "update_global_option_tables") {
	
	fn_echo("Start<br />");

	$global_options = db_get_array("SELECT option_id, options_table FROM ?:product_options WHERE product_id = 0");

	foreach ($global_options as $val) {
		$name = db_get_field("SELECT option_name FROM ?:product_options_descriptions WHERE option_id = ?i", $val['option_id']);
		$options_id = db_get_fields("SELECT option_id FROM ?:product_options_descriptions WHERE option_name = ?s AND option_id <> ?i", $name, $val['option_id']);
		$data = array('options_table' => $val['options_table']);

		db_query("UPDATE ?:product_options SET ?u WHERE option_id IN (?n)", $data, $options_id);
		
		fn_echo("Update $name option<br />");
	}

	fn_echo("Finish<br />");
	
	exit();

} elseif ($mode == "update_option_tables_by_name") {
	
	fn_echo("Start<br />");

    // only for product with 2 options and both options names are same as input names
	// if ( !isset($_REQUEST['rows']) || empty($_REQUEST['rows']) || !isset($_REQUEST['columns']) || empty($_REQUEST['columns'])) {
		
	// 	fn_echo( __('enter_both_values') . '<br />' );
	// 	fn_echo("Finish<br />");

	// 	exit();
	// }

	$rows = $_REQUEST['rows'];
	$columns = $_REQUEST['columns'];
	$categories = !empty($_REQUEST['categories']) ? $_REQUEST['categories'] : '';

	$condition = '';

	if ($categories) {
		$allowed_products = db_get_fields("SELECT product_id FROM ?:products_categories WHERE category_id IN (?n)", explode(',', $categories));
		$condition .= db_quote(" AND product_id IN (?n)", $allowed_products);

	}

	if ($rows) {
	
		$rows_data = db_get_hash_array("SELECT opt.option_id, opt.product_id FROM ?:product_options_descriptions AS descr LEFT JOIN ?:product_options AS opt ON opt.option_id = descr.option_id WHERE descr.option_name LIKE (?l) AND lang_code = ?s $condition", 'product_id', "%$rows%", CART_LANGUAGE);

		if (!empty($rows_data)) {
			
			foreach ($rows_data as $product_id => $option) {

				//	both names are the same
				// if (!isset($columns_data[$product_id])) {
				// 	continue;				  
				// }

				//	only for a product with 2 options
				// if ( db_get_field("SELECT COUNT(option_id) FROM ?:product_options WHERE product_id = ?i", $product_id) != 2 ) {
				// 	continue;				  
				// }

				db_query("UPDATE ?:product_options SET ?u WHERE option_id = ?i", array('options_table' => 'R'), $option['option_id']);
	  
				fn_echo("Update product: ID #" . $product_id . " (set: Rows)<br />");

			}

		} else {
			fn_echo("No Rows options!<br />");
		}
	}

	if ($columns) {
	
		$columns_data = db_get_hash_array("SELECT opt.option_id, opt.product_id FROM ?:product_options_descriptions AS descr LEFT JOIN ?:product_options AS opt ON opt.option_id = descr.option_id WHERE descr.option_name LIKE (?l) AND lang_code = ?s $condition", 'product_id', "%$columns%", CART_LANGUAGE);

		if (!empty($columns_data)) {
			
			foreach ($columns_data as $product_id => $option) {

				db_query("UPDATE ?:product_options SET ?u WHERE option_id = ?i", array('options_table' => 'C'), $option['option_id']);

				fn_echo("Update product: ID #" . $product_id . " (set: Column)<br />");

			}

		} else {
			fn_echo("No column options!<br />");
		}
	}

	fn_echo("Fin<br />");
	
	fn_echo("<br /><a href=" . fn_url("product_options.pre_update_option_tables_by_name"). ">Back</a><br />");

	exit();

} elseif ($mode == "update_options_min_qty") {
	
	fn_echo("Start..<br />");

	$result = db_query("UPDATE ?:products SET options_min_qty = min_qty WHERE min_qty > 0 AND options_min_qty <> min_qty");

	if ( $result > 0 ) {
		fn_echo("Update: " . $result . " product(s)<br />");
	} else {
		fn_echo("Not product to update<br />");
	}

	fn_echo("Finish<br />");
	
	exit();

} elseif ($mode == "show_modifier_in_table") {

	fn_echo("Start..<br />");

	if (isset($_REQUEST['status'])) {
	
		$status = $_REQUEST['status'] == 'N' ? 'N' : 'Y';
		db_query("UPDATE ?:product_options SET ?u", array('show_modifier_in_table' => $status));
	}

	fn_echo("Finish<br />");
	exit();

}