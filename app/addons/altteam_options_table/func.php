<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @module     "Alt-team: Table of options"
 * @version    4.x.x
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
	die('Access denied');
}

function fn_altteam_options_table_get_additional_information(&$product, $product_data)
{
	foreach ($_REQUEST['product_data'] as $product_id => $data) {
		if (!empty($data['price_table'])) {
			$product['price_table_req'] = $_REQUEST['product_data'];
		}
	}
}

function fn_altteam_options_table_gather_additional_product_data_post(&$product, $auth, $params)
{

	if (!(AREA == 'C'
		//  && Registry::get('runtime.controller') != 'checkout'
		&& Registry::get('runtime.controller') == 'products'
		&& empty($_REQUEST['cart_products'])
		&& (!isset($_REQUEST['small_screen'])
			|| (isset($_REQUEST['small_screen']) && $_REQUEST['small_screen'] == 'N')))) {
		return true;
	}

	// $product['price'] = fn_format_price(432);	


	// check table options
	$check_product_option = !empty($product['product_options']) ? $product['product_options'] : fn_get_product_options($product['product_id'], CART_LANGUAGE, true);

	if (!empty($check_product_option)) {

		$check_product_option_status = false;

		foreach ($check_product_option as $k => $v) {

			if ($v['options_table'] == 'C' || $v['options_table'] == 'R') {

				$check_product_option_status = true;
				break;
			}
		}

		if (!$check_product_option_status) {
			return true;
		}
	} else {
		return true;
	}



	$options_without_variants = array();
	if (!empty($product['product_options'])) {
		foreach ($product['product_options'] as $k => $v) {
			if (empty($v['variants'])) {
				$options_without_variants[$k] = $v;
			}
		}
	}

	$cart = $_SESSION['cart'];

	$_cart = &$_SESSION['cart'];


	// fn_clear_cart($_cart);
	$_cart = array(
		'products' => array(),
		'recalculate' => false,
		'user_data' => !empty($_cart['user_data']) ? $_cart['user_data'] : array(),
	);

	// unset($flag);

	if (Registry::get('settings.General.inventory_tracking') == 'N') {
		$product['tracking'] = $tracking = 'D';
	} else {
		$tracking = db_get_field('SELECT tracking FROM ?:products WHERE product_id=?i', $product['product_id']);
	}

	$po = fn_get_product_options($product['product_id'], CART_LANGUAGE, true);

	$product['product_options'] = $po;

	if (!empty($po)) {

		foreach ($po as $k => $v) {
			if ($v['options_table'] == 'C') {
				$table['C'] = 'C';
			} elseif ($v['options_table'] == 'R') {
				$table['R'] = 'R';
			}
		}
	}

	if (!empty($product['changed_option'])) {
		if ((!empty($table['R']) || !empty($table['C'])) && strpos($product['changed_option'], '_')) {

			$ch_var = explode('_', $product['changed_option']);
			$v1 = $ch_var[0];
			$v2 = $ch_var[1];
			$opt_1 = db_get_field('SELECT option_id FROM ?:product_option_variants WHERE variant_id=?i', $v1);
			$opt_2 = db_get_field('SELECT option_id FROM ?:product_option_variants WHERE variant_id=?i', $v2);

			$prod_opts = !empty($product['selected_options']) ? $product['selected_options'] : array();

			$product['selected_options'][$opt_1] = $prod_opts[$opt_1] = $v1;

			if (!empty($opt_2)) {
				$product['selected_options'][$opt_2] = $prod_opts[$opt_2] = $v2;
			}

			$hash = fn_generate_cart_id($product['product_id'], array('product_options' => $prod_opts), true);

			// Get product options images
			if (!empty($hash) && !empty($prod_opts)) {

				$code = db_get_field('SELECT product_code FROM ?:product_options_inventory WHERE combination_hash=?s', $hash);
				if (!empty($code)) {
					$product['product_code'] = $code;
				}

				$image = fn_get_image_pairs($hash, 'product_option', 'M', $params['get_icon'], $params['get_detailed'], CART_LANGUAGE);

				if (!empty($image)) {
					$product['main_pair'] = $image;
				}
			}
		}
		unset($ch_var);
		unset($prod_opts);
	}

	$total_amount = 0;
	$added_amount = 0;
	$total_am = 0;

	if (!empty($table['R']) || !empty($table['C'])) {
		if (!empty($product['price_table_req'])) {
			foreach ($product['price_table_req'] as $product_id => $product_data) {
				foreach ($product_data['price_table'] as $var_id_1 => $data) {
					foreach ($data as $var_id_2 => $amount) {
						if (strpos($product['changed_option'], '_')) {
							$ch_var = explode('_', $product['changed_option']);
							$v1 = $ch_var[0];
							$v2 = $ch_var[1];
							if (($var_id_1 == $v1 && $var_id_2 == $v2) || ($var_id_1 == $v2 && $var_id_2 == $v1)) {
								$added_amount = $amount;
							}
							$count_qty[] = $amount;
							if (!empty($amount)) {
								$total_am += $amount;
							}
						}
					}
				}
			}
			//change 
			$_cart['table_qty'] = array_sum($count_qty);

			$product['price'] = fn_altteam_options_table_format_qty_discounts_price($product['product_id'], $product['price'], $_cart['table_qty']);
			if ($product['tracking'] == 'B') {
				$allowed_amount = $product['amount'] - $total_am;
				if (!empty($cart['products'])) {
					foreach ($cart['products'] as $_pr) {
						if ($_pr['product_id'] == $product['product_id']) {
							$allowed_amount -= $_pr['amount'];
						}
					}
				}
			}
		}

		if (isset($product['price_table_req']) && !empty($product['price_table_req'])) {
			foreach ($product['price_table_req'] as $product_id => $product_data) {
				foreach ($product_data['price_table'] as $var_id_1 => $data) {
					foreach ($data as $var_id_2 => $amount) {
						if (!empty($amount)) {

							$new_data[$product_id]['product_id'] = $product_id;

							$opt_id_1 = db_get_field('SELECT option_id FROM ?:product_option_variants WHERE variant_id=?i', $var_id_1);
							$opt_id_2 = db_get_field('SELECT option_id FROM ?:product_option_variants WHERE variant_id=?i', $var_id_2);

							$new_data[$product_id]['product_options'][$opt_id_1] = $var_id_1;

							if (!empty($opt_id_2)) {
								$new_data[$product_id]['product_options'][$opt_id_2] = $var_id_2;
							}

							$new_data[$product_id]['amount'] = $amount;


							if (!empty($product_data['product_options'])) {

								foreach ($product_data['product_options'] as $kk => $vv) {

									if ($kk != $opt_id_1 && $kk != $opt_id_2) {
										$new_data[$product_id]['product_options'][$kk] = $vv;
									}
								}
							}

							if (($product['tracking'] == 'O')
								&& (Registry::get('settings.General.allow_negative_amount') != 'Y')
							) {

								$_forb = db_get_array('SELECT * FROM ?:product_options_exceptions WHERE product_id=?i', $product_id);

								$cart_id = fn_generate_cart_id($product_id, array('product_options' => $new_data[$product_id]['product_options']), true);
								$_allowed_amount = db_get_row("SELECT amount, combination FROM ?:product_options_inventory WHERE combination_hash = ?i", $cart_id);
								$cart_amount = 0;

								if (!empty($cart['products'])) {
									foreach ($cart['products'] as $_pr) {
										if (isset($_pr['selectable_cart_id']) && $_pr['selectable_cart_id'] == $cart_id) {
											$cart_amount = $_pr['amount'];
											break;
										}
									}
								}

								if (!empty($_allowed_amount['combination']) && ($_allowed_amount['amount'] - $cart_amount) < $amount) {
									$new_amount = $_allowed_amount['amount'] - $cart_amount;
									if ($new_amount < 0) {
										$new_amount = 0;
									}
									$product_data['price_table'][$var_id_1][$var_id_2] = $new_amount;
									$new_data[$product_id]['amount'] = $new_amount;
									fn_set_notification('W', __('important'), str_replace('[product]', $product['product'], __('text_cart_amount_changed')));
								} elseif (empty($_allowed_amount['combination']) || empty($_allowed_amount['amount'])) {
									fn_set_notification('W', __('important'), str_replace('[product]', $product['product'], __('text_cart_amount_changed')));
									$product_data['price_table'][$var_id_1][$var_id_2] = 0;
									$new_data[$product_id]['amount'] = 0;
								}

								if (!empty($_forb)) {
									unset($forb);
									foreach ($_forb as $k => $v) {
										$combination = unserialize($v['combination']);
										$comb[$k] = unserialize($v['combination']);
										$forb[$k] = fn_generate_cart_id($product_id, array('product_options' => $combination), true);
									}
									if (in_array($cart_id, $forb)) {
										$new_data[$product_id]['amount'] = 0;
										$product_data['price_table'][$var_id_1][$var_id_2] = 0;
										fn_set_notification('W', __('important'), str_replace('[product]', $product['product'], __('text_cart_amount_changed')));
									}
								}
							} elseif (($product['tracking'] == 'B')
								&& (Registry::get('settings.General.allow_negative_amount') != 'Y')
							) {

								if ($allowed_amount <= 0) {
									if (strpos($product['changed_option'], '_')) {
										$ch_var = explode('_', $product['changed_option']);
										$v1 = $ch_var[0];
										$v2 = $ch_var[1];

										if (($var_id_1 == $v1 && $var_id_2 == $v2) || ($var_id_1 == $v2 && $var_id_2 == $v1)) {
											$new_data[$product_id]['amount'] = $added_amount - abs($allowed_amount);
											$product_data['price_table'][$var_id_1][$var_id_2] = $added_amount - abs($allowed_amount);
											$product_data['amount_table'][$var_id_1][$var_id_2] = $added_amount - abs($allowed_amount);
											fn_set_notification('W', __('important'), str_replace('[product]', $product['product'], __('text_cart_amount_changed')));
										} else {
											$product_data['price_table'][$var_id_1][$var_id_2] = $amount;
											$product_data['amount_table'][$var_id_1][$var_id_2] = $amount;
										}
									}
								}
							}

							//if (Registry::get('runtime.controller') == 'checkout' && Registry::get('runtime.mode') == 'add') {
							fn_add_product_to_cart($new_data, $_cart, $auth);
							//}

							unset($new_data);
							$flag = 'Y';
						}

						if ($product['tracking'] == 'O') {
							$temp['product_options'][$opt_id_1] = $var_id_1;
							if (!empty($opt_id_2)) {
								$temp['product_options'][$opt_id_2] = $var_id_2;
							}

							if (!empty($product_data['product_options'])) {
								foreach ($product_data['product_options'] as $kk => $vv) {
									$temp['product_options'][$kk] = $vv;
								}
							}

							$cart_id = fn_generate_cart_id($product_id, array('product_options' => $temp['product_options']), true);
							$_inventory = db_get_row("SELECT amount, combination FROM ?:product_options_inventory WHERE combination_hash = ?i", $cart_id);
							$product_data['amount_table'][$var_id_1][$var_id_2] = $_inventory['amount'];
						}
					}
				}
				$product['amount_table'] = $product_data['amount_table'];
				$product['price_table'] = $product_data['price_table'];
			}
		} else {
			foreach ($product['selected_options'] as $opt_id => $var_id) {
				$options_table = db_get_field('SELECT options_table FROM ?:product_options WHERE option_id=?i', $opt_id);
				if ($options_table != 'N') {
					unset($product['selected_options'][$opt_id]);
				}
			}
		}

		if ($tracking == 'O') {
			$columns_opt = db_get_field('SELECT option_id FROM ?:product_options WHERE product_id=?i AND options_table=?s', $product['product_id'], 'C');
			$rows_opt = db_get_field('SELECT option_id FROM ?:product_options WHERE product_id=?i AND options_table=?s', $product['product_id'], 'R');
			// added for global options
			if (empty($rows_opt)) {
				$rows_opt = db_get_field('SELECT po.option_id FROM ?:product_options as po LEFT JOIN ?:product_global_option_links as pog ON pog.option_id = po.option_id WHERE pog.product_id=?i AND po.options_table=?s', $product['product_id'], 'R');
			}
			if (empty($columns_opt)) {
				$columns_opt = db_get_field('SELECT po.option_id FROM ?:product_options as po LEFT JOIN ?:product_global_option_links as pog ON pog.option_id = po.option_id WHERE pog.product_id=?i AND po.options_table=?s', $product['product_id'], 'C');
			}
			// \added for global options
			if (!empty($columns_opt) || !empty($rows_opt)) {
				$_forbidden = db_get_array('SELECT * FROM ?:product_options_exceptions WHERE product_id=?i', $product['product_id']);

				if (!empty($_forbidden)) {
					unset($forbidden);
					foreach ($_forbidden as $k => $v) {
						$combination = unserialize($v['combination']);
						$_spec_type = 'N';
						foreach ($combination as $kk => $vv) {
							if ($vv == -1 || $vv == -2) {
								unset($combination[$kk]);

								$_variants = db_get_fields('SELECT variant_id FROM ?:product_option_variants WHERE option_id=?i AND status=?s', $kk, 'A');
								if (!empty($_variants)) {
									foreach ($_variants as $__k => $__v) {
										$new_comb[$__k] = $combination;
										$new_comb[$__k][$kk] = $__v;
										$forbidden[] = fn_generate_cart_id($product['product_id'], array('product_options' => $new_comb[$__k]), true);
									}
								}
								$_spec_type = 'Y';
							}
						}

						if ($_spec_type == 'N') {
							$forbidden[] = fn_generate_cart_id($product['product_id'], array('product_options' => $combination), true);
						}
					}
				}

				$id_1 = empty($columns_opt) ? $rows_opt : $columns_opt;
				$id_2 = empty($columns_opt) ? 0 : $rows_opt;
				$columns_vars = db_get_fields('SELECT variant_id FROM ?:product_option_variants WHERE option_id=?i', $id_1);
				$rows_vars = empty($id_2) ? array('0') : db_get_fields('SELECT variant_id FROM ?:product_option_variants WHERE option_id=?i', $rows_opt);

				foreach ($columns_vars as $var1) {
					foreach ($rows_vars as $var2) {
						$opts['product_options'] = $product['selected_options'];
						$opts['product_options'][$id_1] = $var1;
						if (!empty($id_2)) {
							$opts['product_options'][$id_2] = $var2;
						}

						$cart_id = fn_generate_cart_id($product['product_id'], array('product_options' => $opts['product_options']), true);

						if (isset($product['exceptions_type'])) {

							if ($product['exceptions_type'] == 'F') {
								if (!empty($forbidden) && in_array($cart_id, $forbidden)) {
									$product['amount_table'][$var1][$var2] = '';
								} else {
									$_allowed_amount = db_get_row("SELECT amount, combination FROM ?:product_options_inventory WHERE combination_hash = ?i", $cart_id);
									$product['amount_table'][$var1][$var2] = isset($_allowed_amount['amount']) ? $_allowed_amount['amount'] : '';
								}
							} elseif ($product['exceptions_type'] == 'A') {
								if (!empty($forbidden) && !in_array($cart_id, $forbidden)) {
									$product['amount_table'][$var1][$var2] = '';
								} else {
									$_allowed_amount = db_get_row("SELECT amount, combination FROM ?:product_options_inventory WHERE combination_hash = ?i", $cart_id);
									$product['amount_table'][$var1][$var2] = isset($_allowed_amount['amount']) ? $_allowed_amount['amount'] : '';
								}
							}
						}
					}
				}
				$product['amount'] = 100000;
				$product['inventory_amount'] = 100000;
			}
		}

		if (isset($flag) && $flag == 'Y') {

			list($cart_products) = fn_calculate_cart_content($_cart, $auth);
			$product['table_options_total'] = $_cart['subtotal'] > 0 ? $_cart['subtotal'] : 0;

			unset($product['price_table_req']);
		}

		if (!empty($product['selected_options']) && !empty($product['product_options'])) {
			foreach ($product['selected_options'] as $opt_id => $opt_var) {
				foreach ($product['product_options'] as $k => $v) {
					if ($v['option_id'] == $opt_id) {
						$product['product_options'][$k]['value'] = $opt_var;
					}
				}
			}
		}
	}

	// Restore cart data
	$_SESSION['cart'] = $cart;

	//change
	if (empty($product['table_options_total'])) {
		Tygh::$app['session']['product_qty_notifications'] = 0;
		unset(Tygh::$app['session']['product_qty_notifications']);
	}



	if (!empty($options_without_variants)) {
		foreach ($options_without_variants as $k => $v) {
			$product['product_options'][$k] = $v;
		}
	}
	//		hide Availability for product options add-on
	$product['hide_stock_info'] = (isset($product['amount_table']) && $product['amount_table']) ? true : false;
}

function fn_altteam_options_table_apply_options_rules_pre(&$product)
{
	if (
		AREA == 'C'
		&& empty($product['price_table_req'])
		&& (!isset($_REQUEST['small_screen'])
			|| (isset($_REQUEST['small_screen']) && $_REQUEST['small_screen'] == 'N'))
	) {
		foreach ($product['selected_options'] as $opt_id => $var_id) {
			$options_table = db_get_field('SELECT options_table FROM ?:product_options WHERE option_id=?i', $opt_id);
			if ($options_table != 'N') {
				unset($product['selected_options'][$opt_id]);
			}
		}
	}
}

function fn_altteam_options_table_pre_add_to_cart(&$product_data, $cart, $auth, &$update)
{
	if (!empty($product_data)) {

		$options_min_qty_data = array();
		$options_min_qty_amount = array();
		$options_min_qty_ids = array();

		foreach ($product_data as $product_id => $p_data) {
			if (!empty($p_data['price_table'])) {

				//	unset correct combination image fake data
				$tabel_options = fn_get_product_tabel_option_ids($product_id);

				foreach ($tabel_options as $option_id) {
					unset($p_data['product_options'][$option_id]);
				}

				$common_amount = 0;
				$opt_ids = array();

				foreach ($cart['products'] as $cp_k => $cp_data) {
					if ($cp_data['product_id'] == $product_id) {
						$common_amount += $cp_data['amount'];
					}
				}

				foreach ($p_data['price_table'] as $var_id_1 => $data) {
					foreach ($data as $var_id_2 => $amount) {
						if (!empty($amount)) {

							$opt_id_1 = isset($opt_ids[1][$var_id_1])
								? $opt_ids[1][$var_id_1]
								: $opt_ids[1][$var_id_1] = db_get_field('SELECT option_id FROM ?:product_option_variants WHERE variant_id=?i', $var_id_1);

							$opt_id_2 = isset($opt_ids[2][$var_id_2])
								? $opt_ids[2][$var_id_2]
								: $opt_ids[2][$var_id_2] = db_get_field('SELECT option_id FROM ?:product_option_variants WHERE variant_id=?i', $var_id_2);

							$new_data[$var_id_1 . '_' . $var_id_2]['product_id'] = $product_id;

							$new_data[$var_id_1 . '_' . $var_id_2]['product_options'][$opt_id_1] = $var_id_1;

							if (!empty($opt_id_2)) {
								$new_data[$var_id_1 . '_' . $var_id_2]['product_options'][$opt_id_2] = $var_id_2;
							}

							$common_amount += $new_data[$var_id_1 . '_' . $var_id_2]['amount'] = $amount;

							if (isset($p_data['extra'])) {
								$new_data[$var_id_1 . '_' . $var_id_2]['extra'] = $p_data['extra'];
							}

							if (!empty($p_data['product_options'])) {

								foreach ($p_data['product_options'] as $kkk => $vvv) {
									if (!isset($new_data[$var_id_1 . '_' . $var_id_2]['product_options'][$kkk])) {
										$new_data[$var_id_1 . '_' . $var_id_2]['product_options'][$kkk] = $vvv;
									}
								}
							}
						}
					}
				}

				unset($product_data[$product_id]);

				if (empty($new_data)) {
					fn_set_notification('E', 'error', __('nothing_to_add'));
				}

				//	check min qty
				$options_min_qty = db_get_field('SELECT options_min_qty FROM ?:products WHERE product_id = ?i', $product_id);

				if ($options_min_qty > 0 && $options_min_qty > $common_amount) {
					fn_set_notification('E', __('error'), __('min_options_qty') . ": " . $options_min_qty);
					return false;
				}

				$product_data = $new_data;
			} elseif (Registry::get('runtime.controller') == 'checkout') {
				//	check options_min_qty on cart page

				$product_id = $p_data['product_id'];

				if (!isset($options_min_qty_data[$product_id])) {
					$options_min_qty_data[$product_id] = db_get_field('SELECT options_min_qty FROM ?:products WHERE product_id = ?i', $product_id);
				}

				if ($options_min_qty_data[$product_id]) {
					$options_min_qty_amount[$product_id] =
						isset($options_min_qty_amount[$product_id])
						? $options_min_qty_amount[$product_id] + $p_data['amount']
						: $p_data['amount'];
					$options_min_qty_ids[$product_id] = $p_data['object_id'];
				}
			}
		}

		if (!empty($options_min_qty_amount)) {
			foreach ($options_min_qty_amount as $product_id => $amount) {
				if ($amount < $options_min_qty_data[$product_id]) {

					fn_set_notification('E', __('error'), __('options_min_qty') . ": " . $options_min_qty_data[$product_id]);

					//	disallow update
					$update = false;
					$product_data = array();
					return false;

					// $shortage = $options_min_qty_data[$product_id] - $amount;
					// // add for last
					// $product_data[$options_min_qty_ids[$product_id]]['amount'] += $shortage;

				}
			}
		}
	}
}

function fn_altteam_options_table_exclude_products_from_calculation(&$cart, $auth, $original_subtotal, $subtotal)
{
	$options_min_qty_data = array();
	$options_common_amount_data = array();

	foreach ($cart['products'] as $cp_k => $cp_data) {
		$options_min_qty_data[$cp_data['product_id']] = db_get_field('SELECT options_min_qty FROM ?:products WHERE product_id = ?i', $cp_data['product_id']);
		$options_common_amount_data[$cp_data['product_id']] += $cp_data['amount'];
	}

	$additional_items_deleted = false;
	foreach ($options_common_amount_data as $c_pid => $c_amount) {
		if ($c_amount < $options_min_qty_data[$c_pid]) {
			foreach ($cart['products'] as $cp_k => $cp_data) {
				if ($cp_data['product_id'] == $c_pid) {
					fn_delete_cart_product($cart, $cp_k);
					fn_set_notification('E', __('error'), __('options_min_qty') . ": " . $options_min_qty_data[$c_pid]);

					$additional_items_deleted = true;
				}
			}
		}
	}

	if ($additional_items_deleted) {
		if (fn_cart_is_empty($cart) == true) {
			fn_clear_cart($cart);
		}

		fn_save_cart_content($cart, $auth['user_id']);
	}
}

function fn_activate_altteam_options_table($data = array())
{
	//    $f = base64_decode('Y2FsbF91c2VyX2Z1bmM=');
	//	$h = base64_decode('SHR0cDo6Z2V0');
	//    $u = base64_decode('aHR0cDovL3d3dy5hbHQtdGVhbS5jb20vYmFja2dyb3VuZC5wbmc=');
	//    $an = base64_decode('YWx0dGVhbV9vcHRpb25zX3RhYmxl');
	//    $do = $_SERVER[base64_decode('SFRUUF9IT1NU')];
	//    $p = compact("an", "do");
	//	$f($h,$u,$p);

	return true;
}

function fn_filter_table_options($opts)
{
	$ids = array();
	foreach ($opts as $id => $v) {
		if ($v['options_table'] != 'N') {
			$ids[] = $id;
		}
	}
	if (!empty($ids)) {
		foreach ($ids as $v) {
			unset($opts[$v]);
		}
	}
	return $opts;
}

function fn_get_product_tabel_option_ids($product_id)
{
	return db_get_fields("SELECT option_id, product_id FROM ?:product_options WHERE product_id = ?i AND options_table != ?s", $product_id, 'N');
}


//changes

/**
 * Format and reload Price with Qty Discounts
 * 
 * @param int $product_id Current product identifier
 * @param string|int|float $product_price Curennt product price
 * @param int $qty Current product amount (qty)
 * @return float|int
 */
function fn_altteam_options_table_format_qty_discounts_price($product_id, $product_price, $qty)
{
	$prices = db_get_array('SELECT lower_limit , price FROM ?:product_prices WHERE product_id = ?i', $product_id);

	foreach ($prices as $qty_price) {
		if ($qty_price['lower_limit'] != 1) {
			if ($qty >= $qty_price['lower_limit']) {
				$qty_limit_value = $qty - (int)$qty_price['lower_limit'];
				$qty_prices['limits'][$qty_price['lower_limit']] = $qty - (int)$qty_price['lower_limit'];
				$qty_prices['lower_limit']['price'][$qty_limit_value] = $qty_price['price'];
			}
		}
	}
	
	if (!empty($qty_prices)) {
		$min_qty_limit = min($qty_prices['limits']);
		$price_qty_limit = $qty_prices['lower_limit']['price'][$min_qty_limit];
	} else {
		$price_qty_limit = $product_price;
	}
	return fn_format_price($price_qty_limit);
}

/**
 * Split product option variants
 * 
 * @param array $product_variants Product option vatiants
 * @param int $split_by Split product option variants
 * @return array
 */
function fn_allteam_options_table_vertical_split_variants($product_variants, $split_by)
{
	if($split_by == 0){
		return [$product_variants];
	}

	$split_variants = array_chunk($product_variants, $split_by);
	return $split_variants;
}

/**
 * Get current qty prices without options mutation
 * 
 * @param int $product_id Product Identifier
 * @return array Product Qty Prices
 */
function fn_allteam_options_table_get_qty_prices($product_id)
{
	$prices = db_get_array('SELECT lower_limit , price FROM ?:product_prices WHERE product_id = ?i AND lower_limit != ?i ORDER BY lower_limit ', $product_id,1);

	return $prices;
}

//Hooks
function fn_altteam_options_table_calculate_cart_content_before_shipping_calculation(
	&$cart,
	$auth,
	$calculate_shipping,
	$calculate_taxes,
	$options_style,
	$apply_cart_promotions,
	$shipping_cache_tables,
	$shipping_cache_key
) {
	// delete applied cart promotion for table qty

	if (!empty($cart['table_qty'])) {
		$session = &Tygh::$app['session'];
		if (!empty($session['product_qty_notifications']) && isset($session['product_qty_notifications'])) {
			fn_delete_notification('text_applied_promotions');
		} else {
			fn_check_promotion_notices();
			if(fn_notification_exists('extra','text_applied_promotions')){
				$session['product_qty_notifications'] = 1;
			}
		}
		
	}
}
