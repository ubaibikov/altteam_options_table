<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @module     "Alt-team: Table of options"
 * @version    4.x.x 
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

fn_register_hooks(
	'pre_add_to_cart',
	'exclude_products_from_calculation',
	'get_additional_information', 
	'gather_additional_product_data_post',
	'apply_options_rules_pre',
	'calculate_cart_content_before_shipping_calculation'
);