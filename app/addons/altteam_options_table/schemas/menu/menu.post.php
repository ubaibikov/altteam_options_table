<?php

$schema['central']['website']['items']['option_table_update'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'product_options.pre_update_option_tables_by_name',
    'position' => 700
);

return $schema;