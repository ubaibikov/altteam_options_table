{capture name="mainbox"}

{capture name="tabsbox"}

<div id="pre_update_option_tables_by_name">

<form action="{""|fn_url}" method="POST" name="update_option_tables_by_name">

	<div class="control-group">
		<label for="table_rows" class="control-label">{__("table_rows")}:</label>
		<div class="controls">
			<input type="text" name="table[R]" id="table_rows" value="" size="40" class="span5" />
		</div>
	</div>

	<div class="control-group">
		<label for="table_columns" class="control-label">{__("table_columns")}:</label>
		<div class="controls">
			<input type="text" name="table[C]" id="table_columns" value="" size="40" class="span5" />
		</div>
	</div>

    {include file="pickers/categories/picker.tpl" input_name="table[categories]" multiple=true no_item_text="Select all"}

	<div class="buttons-container">
		{include file="buttons/save.tpl" but_text=$lang.update but_name="dispatch[product_options.update_option_tables_by_name]"}
	</div>

</form>

<!--pre_update_option_tables_by_name--></div>

{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}
{include file="common/mainbox.tpl" title=__("pre_update_option_tables_by_name") content=$smarty.capture.mainbox}