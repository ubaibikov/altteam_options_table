<div class="control-group">
	<label class="control-label" for="options_table_{$id}">{__("options_table")}:</label>
	<div class="controls">
	<select id="options_table_{$id}" name="option_data[options_table]">
		<option value="N" {if $option_data.options_table == "N"}selected="selected"{/if}>{__('none')}</option>
		<option value="R" {if $option_data.options_table == "R"}selected="selected"{/if}>{__('table_rows')}</option>
		<option value="C" {if $option_data.options_table == "C"}selected="selected"{/if}>{__('table_columns')}</option>
	</select>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="show_modifier_in_table_{$id}">{__("show_modifier_in_table")}:</label>
    <input type="hidden" name="option_data[show_modifier_in_table]" value="N" />
    <div class="controls">
    <label class="checkbox">
        <input type="checkbox" name="option_data[show_modifier_in_table]" id="show_modifier_in_table_{$id}" value="Y" {if $option_data.show_modifier_in_table == "Y"}checked="checked"{/if}/>
    </label>
    </div>
</div>

{*
	change
*}
<div class="control-group">
	<label class="control-label" for="show_modifier_in_table_{$id}">{__("split_by")}:</label>
    <div class="controls">
    <label class="checkbox">
        <input type="text" name="option_data[split_by]" id="split_by_in_table_{$id}" value="{$option_data.split_by}"/>
    </label>
    </div>
</div>