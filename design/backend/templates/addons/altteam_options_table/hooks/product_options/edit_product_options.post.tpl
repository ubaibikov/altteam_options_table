<div class="control-group cm-non-cb">
    <label class="control-label">{__("only_qty")}</label>
    <div class="controls">
        <input type="hidden" name="option_data[variants][{$num}][only_qty]" value="N" />
		<label class="checkbox">
                <input type="checkbox" name="option_data[variants][{$num}][only_qty]" id="elm_inventory_only_qty_{$num}" value="Y" {if $vr.only_qty == "Y"}checked="checked"{/if}/>
		</label>
    </div>
</div>