	
{include file="common/subheader.tpl" title=__("options_table") target="#acc_options_table_type"}

<div id="acc_options_table_type">
	  
	<div class="control-group">
	    <label class="control-label" for="elm_options_min_qty">{__("options_min_qty")}:</label>
	    <div class="controls">
	        <input type="text" name="product_data[options_min_qty]" size="10" id="elm_options_min_qty" value="{$product_data.options_min_qty|default:"0"}" class="input-small" />
	    </div>
	</div>

</div>