
{assign var="value" value=$product.price_table[$var1][$var2]}

{if $product.amount_table}
	{assign var="amount_value" value=$product.amount_table[$var1][$var2]}
{/if}

<td class="options-table{if $product.tracking != 'D' && $product.amount_table && !$amount_value} disabled{/if}">
	<input  class="input-options-table {if $value}product-option-background{/if}" id="{$var1}_{$var2}" type="text" size="3" onkeypress="fn_to_change_options('{$obj_prefix}{$obj_id}', '{$obj_id}', '{$var1}_{$var2}');" onfocus="fn_clear_field(this);" onblur="fn_restore_field(this);" {if  $product.tracking != 'D' && $product.amount_table && !$amount_value} disabled="disabled"{/if} value="{if $value}{$value}{else}0{/if}" name="product_data[{$obj_id}][price_table][{$var1}][{$var2}]"/>
</td>