{foreach from=$product.product_options item="po"}
	{if $po.options_table == 'C'} 
		{assign var="horizontal" value=$po}
		{assign var="columns_name" value=$po.option_name}
		{$spilt_options = fn_allteam_options_table_vertical_split_variants($po.variants,$po.split_by)}
		{$split_by = $po.split_by}
	{elseif $po.options_table == 'R'} 
		{assign var="vertical" value=$po}
		{assign var="rows_name" value=$po.option_name}
		{$spilt_options = fn_allteam_options_table_vertical_split_variants($po.variants,$po.split_by)}
	{/if}
{/foreach}
	
{if $horizontal || $vertical}

	{if $disable_ids}
		{assign var="_disable_ids" value="`$disable_ids``$obj_id`"}
	{else}
		{assign var="_disable_ids" value=""}
	{/if}


	{include file="views/products/components/product_options.tpl"
		id=$obj_id
		product_options=$product.product_options
		name="product_data"
		capture_options_vs_qty=$capture_options_vs_qty
		disable_ids=$_disable_ids
	}

	<input type="hidden" id="small_screen_{$obj_id}" name="small_screen" value="N" />

	<div class="options-table-div" id="cart_status_opt_table_{$obj_prefix}{$obj_id}">

		{if $horizontal}
			<input type="hidden" name="product_data[{$obj_id}][product_options][{$horizontal.option_id}]" value="{if $horizontal.value}{$horizontal.value}{else}{$horizontal.variants|key}{/if}" />
		{/if}
		{if $vertical}
			<input type="hidden" name="product_data[{$obj_id}][product_options][{$vertical.option_id}]" value="{if $vertical.value}{$vertical.value}{else}{$vertical.variants|key}{/if}" />
		{/if}

		<input type="hidden" name="object_id" value="{$obj_prefix}{$obj_id}" />
		{if $vertical}
			<div class="split-table" style="display:inline-flex;">
				{foreach $spilt_options as $vm}
					<table cellpadding="0" cellspacing="0" border="0" name="option_table">
					<tbody style="
							{if $addons.altteam_options_table.max_scroll_height}
								max-height: {$addons.altteam_options_table.max_scroll_height}px;
							{/if}
							{if $addons.altteam_options_table.max_scroll_width}
								max-width: {$addons.altteam_options_table.max_scroll_width}px;
							{/if}
					">
						<tr>	
							{if $vertical && !$horizontal}
								<td class="option-table-header">{$rows_name}</td>
								<td class="option-table-header">{__('qty')}</td>
							{else}
								{if $vertical && $horizontal}
									<td class="option-table-header">{$rows_name}\{$columns_name}</td>
								{/if}
								{foreach name="horizontal_variants" from=$horizontal.variants item="hor"}
									<td {if $horizontal.show_modifier_in_table == 'Y'}valign="top"{/if} class="option-table-header">
									{if $hor.image_pair.image_id}
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td>
													{if $hor.variant_id == $selected_variant}{assign var="_class" value="product-variant-image-selected"}{else}{assign var="_class" value="product-variant-image-unselected"}{/if}
													{include file="common/image.tpl" class="hand $_class object-image float-left" show_thumbnail="Y" images=$hor.image_pair object_type="product_option" image_width="20" image_height="20" obj_id="variant_image_`$obj_prefix``$obj_id`_`$horizontal.option_id`_`$hor.variant_id`"}

												</td>
												<td style="vertical-align: middle; padding-left: 5px;">
													<span class="float-left color-name-text-with-img">
														{$hor.variant_name}
													</span>
													{if $horizontal.show_modifier_in_table == 'Y' && $hor.modifier|floatval}
														<p>
															({include file="common/modifier.tpl" mod_type=$hor.modifier_type mod_value=$hor.modifier display_sign=true})
														</p>
													{/if}
												</td>
											</tr>
										</table>
									{else}
										<span class="float-left color-name-text">
											{$hor.variant_name}
										</span>
										{if $horizontal.show_modifier_in_table == 'Y' && $hor.modifier|floatval}
											<p>
												({include file="common/modifier.tpl" mod_type=$hor.modifier_type mod_value=$hor.modifier display_sign=true})
											</p>
										{/if}
									{/if}
									</td>
								{/foreach}
							{/if}
						</tr>
						{if $horizontal && !$vertical}
							<tr>
							{foreach name="horizontal_variants" from=$horizontal.variants item="hor"}
								{include file="addons/altteam_options_table/components/ceil.tpl" var1=$hor.variant_id var2=0}
							{/foreach}
							</tr>
						{else}
								{foreach name="vertical_variants" from=$vm item="vert" key="split_table"}
								<tr>
									<td class="option-table-vertical">
										{if $vert.image_pair.image_id}
											<table cellpadding="0" cellspacing="0" border="0">
												<tr>
													<td>
														{if $vert.variant_id == $selected_variant}{assign var="_class" value="product-variant-image-selected"}{else}{assign var="_class" value="product-variant-image-unselected"}{/if}
														{include file="common/image.tpl" class="hand $_class object-image float-left" show_thumbnail="Y" images=$vert.image_pair object_type="product_option" image_width="20" image_height="20" obj_id="variant_image_`$obj_prefix``$obj_id`_`$vertical.option_id`_`$vert.variant_id`"}
													</td>
													<td class="option-tabel-row-name">
														<span class="float-left color-name-text-with-img">
															{$vert.variant_name}
														</span>
														{if $vertical.show_modifier_in_table == 'Y' && $vert.modifier|floatval}
															<p>
																({include file="common/modifier.tpl" mod_type=$vert.modifier_type mod_value=$vert.modifier display_sign=true})
															</p>
														{/if}
													</td>
												</tr>
											</table>
											{else}
												<span class="float-left color-name-text">
													{$vert.variant_name}
												</span>
												{if $vertical.show_modifier_in_table == 'Y' && $vert.modifier|floatval}
													<p>
														({include file="common/modifier.tpl" mod_type=$vert.modifier_type mod_value=$vert.modifier display_sign=true})
													</p>
												{/if}
										{/if}
									</td>

									{if $vertical && !$horizontal}
											<span class="float-left">
											{include file="addons/altteam_options_table/components/ceil.tpl" var1=$vert.variant_id var2=0}
											<span class="float-left">
									{else}
										{foreach name="horizontal_variants" from=$horizontal.variants item="hor"}
											{include file="addons/altteam_options_table/components/ceil.tpl" var1=$hor.variant_id var2=$vert.variant_id}
										{/foreach}
									{/if}
								</tr>
								{/foreach}
						{/if}

					</tbody>
					</table>
				{/foreach}
			</div>
		{/if}
		<style>
			.some-table{
				margin-bottom: 2%;
			}
		</style>
		{if $horizontal}
			{foreach $spilt_options as $vm}
			{if $split_by > 0}
						{$split_width = 100 / $split_by}
						{else}
						{$split_width = 100}
			{/if}
				<table cellpadding="0" cellspacing="0" border="0" name="option_table" class="some-table" style="width:{$split_width}%">
					<tbody style="
							{if $addons.altteam_options_table.max_scroll_height}
								max-height: {$addons.altteam_options_table.max_scroll_height}px;
							{/if}
							{if $addons.altteam_options_table.max_scroll_width}
								max-width: {$addons.altteam_options_table.max_scroll_width}px;
							{/if}
							
					" class="some-table-tbody">
						<tr style="">
						{if $vertical && !$horizontal}
							<td class="option-table-header">{$rows_name}</td>
							<td class="option-table-header">{__('qty')}</td>
						{else}
							{if $vertical && $horizontal}
								<td class="option-table-header">{$rows_name}\{$columns_name}</td>
							{/if}
							{foreach name="horizontal_variants" from=$vm item="hor"}
							<td {if $horizontal.show_modifier_in_table == 'Y'}valign="top"{/if} class="option-table-header">
								{if $hor.image_pair.image_id}
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
										<td style="vertical-align: middle; padding-left: 5px; ">
												<span> 
													{if $hor.variant_id == $selected_variant}{assign var="_class" value="product-variant-image-selected"}{else}{assign var="_class" value="product-variant-image-unselected"}{/if}
													{include file="common/image.tpl" class="hand $_class object-image float-left" show_thumbnail="Y" images=$hor.image_pair object_type="product_option" image_width="20" image_height="20" obj_id="variant_image_`$obj_prefix``$obj_id`_`$horizontal.option_id`_`$hor.variant_id`"}
												</span>
												<span class="float-left color-name-text-with-img">
													{$hor.variant_name}
												</span>
												{if $horizontal.show_modifier_in_table == 'Y' && $hor.modifier|floatval}
													<p>
														({include file="common/modifier.tpl" mod_type=$hor.modifier_type mod_value=$hor.modifier display_sign=true})
													</p>
												{/if}
											</td>
										</tr>
									</table>
								{else}
									<span class="float-left color-name-text " >
										{$hor.variant_name}
									</span>
									{if $horizontal.show_modifier_in_table == 'Y' && $hor.modifier|floatval}
										<p>
											({include file="common/modifier.tpl" mod_type=$hor.modifier_type mod_value=$hor.modifier display_sign=true})
										</p>
									{/if}
								{/if}
								</td>
							{/foreach}
						{/if}
						</tr>
						{if $horizontal && !$vertical}
							<tr>
							{foreach name="horizontal_variants" from=$vm item="hor"}
								{include file="addons/altteam_options_table/components/ceil.tpl" var1=$hor.variant_id var2=0}
							{/foreach}
							</tr>
						{else}
							{foreach name="vertical_variants" from=$horizontal.variants item="vert" key="split_table"}
								<tr>
									<td class="option-table-vertical">
										{if $vert.image_pair.image_id}
											<table cellpadding="0" cellspacing="0" border="0">
												<tr>
													<td>
														{if $vert.variant_id == $selected_variant}{assign var="_class" value="product-variant-image-selected"}{else}{assign var="_class" value="product-variant-image-unselected"}{/if}
														{include file="common/image.tpl" class="hand $_class object-image float-left" show_thumbnail="Y" images=$vert.image_pair object_type="product_option" image_width="20" image_height="20" obj_id="variant_image_`$obj_prefix``$obj_id`_`$vertical.option_id`_`$vert.variant_id`"}
													</td>
													<td class="option-tabel-row-name">
														<span class="float-left color-name-text-with-img">
															{$vert.variant_name}
														</span>
														{if $vertical.show_modifier_in_table == 'Y' && $vert.modifier|floatval}
															<p>
																({include file="common/modifier.tpl" mod_type=$vert.modifier_type mod_value=$vert.modifier display_sign=true})
															</p>
														{/if}
													</td>
												</tr>
											</table>
											{else}
												<span class="float-left color-name-text">
													{$vert.variant_name}
												</span>
												{if $vertical.show_modifier_in_table == 'Y' && $vert.modifier|floatval}
													<p>
														({include file="common/modifier.tpl" mod_type=$vert.modifier_type mod_value=$vert.modifier display_sign=true})
													</p>
												{/if}
										{/if}
									</td>
										{foreach name="horizontal_variants" from=$horizontal.variants item="hor"}
											{include file="addons/altteam_options_table/components/ceil.tpl" var1=$hor.variant_id var2=$vert.variant_id}
										{/foreach}
								</tr>
								{/foreach}
						{/if}
					</tbody>
					</table>
			{/foreach}
		{/if}
		<!-- calculate total price -->
		{if !($settings.General.allow_anonymous_shopping == "hide_price_and_add_to_cart" && !$auth.user_id)}
		<div class="ty-control-group product-list-field  ty-product-block__price-actual">

		    <label class="ty-control-group__label ty-price-num">Total<!-- {__("table_options_total")} -->:</label>

		    <div class="ty-control-group__item">

		    	<span class="cm-reload-{$obj_prefix}{$obj_id}">
		            <span class="ty-price" id="line_discounted_price_{$obj_prefix}{$obj_id}">
		            	{include
		            		file="common/price.tpl"
		            		value=$product.table_options_total
		            		span_id="discounted_price_`$obj_prefix``$obj_id`"
		            		class="ty-price-num"
		        		}
		            </span>
			    </span>
		    </div>
		</div>
		{/if}
	<!--cart_status_opt_table_{$obj_prefix}{$obj_id}--></div>
<div class="cm-reload-{$obj_prefix}{$obj_id}" id="">
    {if $product.prices}
	{$qty_prices = fn_allteam_options_table_get_qty_prices($product.product_id)}
       <div class="ty-qty-discount">
    <div class="ty-qty-discount__label">{__("text_qty_discounts")}:</div>
    <table class="ty-table ty-qty-discount__table">
        <thead>
            <tr>
                <th class="ty-qty-discount__td">{__("quantity")}</th>
                {foreach from=$qty_prices  item="price"}
                    <th class="ty-qty-discount__td">{$price.lower_limit}+</th>
                {/foreach}
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="ty-qty-discount__td">{__("price")}</td>
                {foreach from=$qty_prices  item="price"}
                    <td class="ty-qty-discount__td">{include file="common/price.tpl" value=$price.price}</td>
                {/foreach}
            </tr>
        </tbody>
    </table>
</div>
    {/if}
</div>

<script type="text/javascript">
(function(_, $) {

    var max_screen_width = '{$addons.altteam_options_table.max_screen_width|escape:javascript nofilter}',
    id = "{$obj_prefix|escape:javascript}{$obj_id|escape:javascript}";

    $(document).ready(function() {

    	//	hide tabel of option for small screen

		var screen_width = $(window).width();

    	if (screen_width > max_screen_width) {

    		{if $horizontal}
	    		$("#opt_" + id + '_' + {$horizontal.option_id|escape:javascript}).remove();
    		{/if}

    		{if $vertical}
	    		$("#opt_" + id + '_' + {$vertical.option_id|escape:javascript}).remove();
    		{/if}

    		$("#qty_update_" + id).remove();
    		$("#small_screen_" + id).val('N');

    	} else {

    		$("#cart_status_opt_table_" + id).remove();

    		$("#small_screen_" + id).val('Y');

    	}

    });
	//	extend fn_change_options
	fn_change_options = function(_super){
		return function() {

			//	remove empty cells data from AJAX query
			$(".input-options-table").each(
				function(elm){
					if ( $(this).val() == "0" ) {
						$(this).addClass('cm-not-request');
					};
				}
			);

			// return _super.apply(this, arguments);
			return fn_change_table_options.apply(this, arguments);
		}
	}(fn_change_options);

}(Tygh, Tygh.$));
</script>

<script type="text/javascript">
//<![CDATA[
{literal}
var to_delay;
function fn_to_change_options(prefix, obj_id, id)
{
	clearTimeout(to_delay);
	to_delay = setTimeout(function()
	{

		//	remove empty cells data from AJAX query
		$(".input-options-table").each(
			function(elm){
				if ( $(this).val() == "0" ) {
					$(this).addClass('cm-not-request');
				};
			}
		);

		fn_change_table_options(prefix, obj_id, id);

	}, 500);
}
function fn_clear_field(obj)
{
	if ($(obj).val() == 0) {
		$(obj).val('');
	}
}
function fn_restore_field(obj)
{
	if ($(obj).val() == '') {
		$(obj).val('0');
	}
}

// Difference from default "fn_change_options" function
//	remove empty data (.cm-not-request)
//	add post process functions(scrollTop)
function fn_change_table_options(obj_id, id, option_id)
{
    var $ = Tygh.$;
    // Change cart status
    var cart_changed = true;

    var params = [];
    var update_ids = [];
    var cache_query = true;
    var defaultValues = {};

    var scrollTopTable = 0;

    var parents = $('.cm-reload-' + obj_id);

    //	save scrollTop for long table
	if ( $(".options-table-div table tbody").css('overflow-y') == 'auto'
			&& $(".options-table-div table tbody").scrollTop() > 0 ) {
		scrollTopTable = $(".options-table-div table tbody").scrollTop();
	}

    $.each(parents, function(id, parent_elm) {
        var reload_id = $(parent_elm).prop('id');
        update_ids.push(reload_id);

        defaultValues[reload_id] = {};

        var elms = $(':input:not([type=radio]):not([type=checkbox]):not(.cm-not-request)', parent_elm);
        $.each(elms, function(id, elm) {
            if (elm.type != 'submit' && elm.type != 'file' && !($(this).hasClass('cm-hint') && elm.value == elm.defaultValue) && elm.name.length != 0) {
                if (elm.name == 'no_cache' && elm.value) {
                    cache_query = false;
                }
                params.push({name: elm.name, value: elm.value});
            }
        });

        elms = $(':input', parent_elm);
        $.each(elms, function(id, elm) {
            if ($(elm).is('select')) {
                var elm_id = $(elm).prop('id');

                $('option', elm).each(function() {
                    if (this.defaultSelected) {
                        defaultValues[reload_id][elm_id] = this.value;
                    }
                });

            } else if ($(elm).is('input[type=radio], input[type=checkbox]')) {
                defaultValues[reload_id][elm_id] = elm.defaultChecked;
            } else {
                defaultValues[reload_id][elm_id] = elm.defaultValue;
            }

        });
    });

    var radio = $('input[type=radio]:checked, input[type=checkbox]', parents);
    $.each(radio, function(id, elm) {
        if ($(elm).prop('disabled')) {
            return true;
        }
        var value = elm.value;
        if ($(elm).is('input[type=checkbox]:checked')) {
            if (!$(elm).hasClass('cm-no-change')) {
                value = $(elm).val();
            }
        } else if ($(elm).is('input[type=checkbox]')) {
            if (!$(elm).hasClass('cm-no-change')) {
                value = 'unchecked';
            } else {
                value = '';
            }
        }

        params.push({name: elm.name, value: value});
    });

    var url = fn_url('products.options?changed_option[' + id + ']=' + option_id);

    for (var i = 0; i < params.length; i++) {
        url += '&' + params[i]['name'] + '=' + encodeURIComponent(params[i]['value']);
    }

    $.ceAjax('request', url, {
        result_ids: update_ids.join(',').toString(),
        caching: cache_query,
        force_exec: true,
        pre_processing: fn_pre_process_form_files,
        callback: function(data, params) {
            fn_post_process_form_files(data, params);

            var parents = $('.cm-reload-' + obj_id);
            $.each(parents, function(id, parent_elm) {
                if (data.html[$(parent_elm).prop('id')]) {
                    var reload_id = $(parent_elm).prop('id');

                    var elms = $(':input', parent_elm);

                    if (defaultValues[reload_id] != null) {
                        $.each(elms, function(id, elm) {
                            var elm_id = $(elm).prop('id');

                            if (defaultValues[reload_id][elm_id] != null) {
                                if ($(elm).is('select')) {
                                    var selected = {};
                                    var is_selected = false;
                                    $('option', elm).each(function() {
                                        selected[this.value] = this.defaultSelected;
                                        this.defaultSelected = (defaultValues[reload_id][elm_id] == this.value) ? true : false;
                                    });
                                    $('option', elm).each(function() {
                                        this.selected = selected[this.value];
                                        if (this.selected == true) {
                                            is_selected = true;
                                        }
                                    });
                                    if (!is_selected) {
                                        $('option', elm).get(0).selected = true;
                                    }
                                } else if ($(elm).is('input[type=radio], input[type=checkbox]')) {
                                    var checked = elm.defaultChecked;
                                    elm.defaultChecked = defaultValues[reload_id][elm_id];
                                    elm.checked = checked;
                                } else {
                                    var value = elm.defaultValue;
                                    elm.defaultValue = defaultValues[reload_id][elm_id];
                                    elm.value = value;
                                }
                            }
                        });
                    }
                }
            });

			if ( scrollTopTable > 0 ) {
				$(".options-table-div table tbody").scrollTop(scrollTopTable);
			};

        },
        method: 'post'
    });

}

{/literal}
//]]>
</script>
{/if}
